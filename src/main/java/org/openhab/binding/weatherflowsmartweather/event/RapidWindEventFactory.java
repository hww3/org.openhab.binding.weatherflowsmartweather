package org.openhab.binding.weatherflowsmartweather.event;

import org.eclipse.smarthome.core.events.AbstractEventFactory;
import org.eclipse.smarthome.core.events.EventFactory;
import org.openhab.binding.weatherflowsmartweather.model.EventRapidWindMessage;

public interface RapidWindEventFactory extends EventFactory {

}
